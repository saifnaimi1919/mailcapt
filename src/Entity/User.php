<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\Timestamp;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="`user`")
 * @ApiResource(
 *     itemOperations={
 *              "get"={
 *                      "access_control"="is_granted('IS_AUTHENTICATED_FULLY')",
 *                      "normalization_context"={"groups"={"get-user"}}
 *          }
 *     },
 *     collectionOperations={
 *              "post"={
 *                      "normalization_context"={"groups"={"get-user"}}
 *          }
 *     }
 * )
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User implements UserInterface
{
    use Timestamp;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-user","get-mail-box-with-owner"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Groups({"get-user"})
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,}/",
     *     message="password must be more than 7 caracteres with uppercase, lowercase letter and numbers"
     * )
     *
     */
    private $password;

    /**
     * @Assert\NotBlank(groups={"post"})
     * @Assert\Expression(
     *       "this.getPassword() === this.getPasswordConfirm()",
     *          message="does not match"
     *        )
     */
    private $passwordConfirm;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-user","get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-user","get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @ORM\Column(type="boolean", options={"default" : false}, nullable=true)
     */
    private $isDeleted = false;

    /**
     * @ORM\OneToMany(targetEntity=MailBox::class, mappedBy="owner")
     */
    private $mailBoxes;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-user","get-mail-box-with-owner"})
     */
    private $username;

    public function __construct()
    {
        $this->mailBoxes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return Collection|MailBox[]
     */
    public function getMailBoxes(): Collection
    {
        return $this->mailBoxes;
    }

    public function addMailBox(MailBox $mailBox): self
    {
        if (!$this->mailBoxes->contains($mailBox)) {
            $this->mailBoxes[] = $mailBox;
            $mailBox->setOwner($this);
        }

        return $this;
    }

    public function removeMailBox(MailBox $mailBox): self
    {
        if ($this->mailBoxes->removeElement($mailBox)) {
            // set the owning side to null (unless already changed)
            if ($mailBox->getOwner() === $this) {
                $mailBox->setOwner(null);
            }
        }

        return $this;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordConfirm()
    {
        return $this->passwordConfirm;
    }

    /**
     * @param mixed $passwordConfirm
     */
    public function setPasswordConfirm($passwordConfirm): void
    {
        $this->passwordConfirm = $passwordConfirm;
    }
}
