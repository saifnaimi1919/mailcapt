<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MailBoxRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations=
 *     {
 *              "post"=
 *                  {
 *                  "security"="is_granted('IS_AUTHENTICATED_FULLY')"
 *                  },
 *              "get"=
 *                  {
 *                   "security"="is_granted('IS_AUTHENTICATED_FULLY')",
 *                    "normalization_context"=
 *                            {
 *                             "groups"={"get-mail-box-with-owner"}
 *                            }
 *                   }
 *
 *     },
 *     itemOperations=
 *     {
 *                  "get"=
 *                      {
 *                      "security"="is_granted('IS_AUTHENTICATED_FULLY')",
 *                      "normalization_context"=
 *                             {
 *                             "groups"={"get-blog-post-with-author"}
 *                            }
 *                      }
 *      }
 * )
 * @ORM\Entity(repositoryClass=MailBoxRepository::class)
 */
class MailBox
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get-mail-box-with-owner"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $folder;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $archive;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $mailerror;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     */
    private $server;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get-mail-box-with-owner"})
     * @Assert\NotBlank()
     * @Assert\Positive
     */
    private $port;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="mailBoxes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-mail-box-with-owner"})
     */
    private $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFolder(): ?string
    {
        return $this->folder;
    }

    public function setFolder(string $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getArchive(): ?string
    {
        return $this->archive;
    }

    public function setArchive(string $archive): self
    {
        $this->archive = $archive;

        return $this;
    }

    public function getMailerror(): ?string
    {
        return $this->mailerror;
    }

    public function setMailerror(string $mailerror): self
    {
        $this->mailerror = $mailerror;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getServer(): ?string
    {
        return $this->server;
    }

    public function setServer(string $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function setPort(int $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
