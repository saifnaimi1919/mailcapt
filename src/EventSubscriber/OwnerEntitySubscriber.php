<?php

namespace App\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\MailBox;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class OwnerEntitySubscriber implements EventSubscriberInterface
{
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
       return
       [
           KernelEvents::VIEW => ['getAuthenticatedUser', EventPriorities::PRE_WRITE]
       ];
    }

    public function getAuthenticatedUser(ViewEvent $event)
    {
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        $token = $this->tokenStorage->getToken();

        if($token === null ){
            return;
        }
        $owner = $token->getUser();
        if ((!$entity instanceof MailBox ) || (Request::METHOD_POST !== $method && Request::METHOD_PUT !== $method)) {
           return;
        }
        $entity->setOwner($owner);

    }
}